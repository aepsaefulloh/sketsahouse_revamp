const UrlUtils = {
    extractID(uri) {
        return uri.substr(uri.length - 2)
    }
}

export default UrlUtils