export default function({ $axios, store }) {
    if (process.client) {
        $axios.setHeader(store.state.appstore.akey, 'Bearer')
    }
}