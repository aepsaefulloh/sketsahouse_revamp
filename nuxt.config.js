export default {

    // Target: https://go.nuxtjs.dev/config-target
    target: 'static',
    ssr: true,
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'sketsahouse_revamp',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css' },
            { rel: "stylesheet", href: 'https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap' }
        ],
        script: [
            { src: 'bootstrap/js/bootstrap.min.js' },
            { src: 'vendor/popper/popper.min.js' }
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '@/static/bootstrap/css/bootstrap.min.css',
        'swiper/css/swiper.min.css'

    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        { src: '~/plugins/vue-awesome-swiper' },
        { src: '~/plugins/axios' },
        { src: '~/plugins/slugify' },

    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    loading: '~/components/LoadingBar.vue',

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
        '@nuxtjs/auth'
    ],

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        baseURL: process.env.API_URL,
        headers: {
            common: {
                'Token': process.env.SECURE_TOKEN,
                'Content-Type': 'application/json',
            },
        },
    },
    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: { url: 'login', method: 'post', propertyName: 'data.token' },
                    user: { url: 'me', method: 'get', propertyName: 'data' },
                    logout: false
                }
            }
        }
    },




    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {}
}